// Repetition Control Structures

// While loop

/*
	- A while loop takes in an expression/condition
	- If the condition evaluates to be true, the statements inside the code will be executed
	- Syntax:
		while (expression/condition){
			statements
		}
*/ 

let count = 5;

// While the value of count is not equal to zero, it will run.
while (count !==0){

	// The current value of count is printed out.
	console.log("While: " + count);

	// Decreases the value of count by 1 after every iteration.
	count--;
}

// Do While Loop
/*
	-A do-while loop works a lot like the while loop but it guarantees that the code will be executed atleast once.

	- Syntax:
		do {
	statement
		} while (expression/condition)
*/



/*let number = Number(prompt("Give me a number"));

do{
	// The current value of number is printed out.
	console.log("Do while: " + number);

	// Increases the value of number by 1 after every iteration.
	// number = number + 1.
	number +=1;

// Providing a number of 10 or greater will run the code block once, but the loop will stop there.
} while(number < 10);*/



// For loop
/*
	- A for loop is more flexible than while and do-while loops.
	- It consist of three parts.
		1. The initial value which is the starting value. The count will start at the initial value.
		2. the expression/condition that will determine if the loop will run one more time. While this is true, the loop will run.
		3. The finalExpression which determines how the loop will run (increment/decrement)
	- Syntax:
		for(initial; expression/condition; finalExpression){
			statement
		}
*/

/*
	- This loop starts from 0.
	- This loop will run as long as the value of count is less than or equal to zero.
	- This loop adds 1 after every run.
*/
for(let count = 0; count <= 19; count++){
	console.log(count);
}

// Characters in string may be counted using the .length property.
// Strings are special compared to other data types as it has access to function and other pieces of information. Other primitive data doesn't have this.
let myString = "jeru";
console.log(myString.length);

// Accessing elements of a string
// Individual characters of a string may be accessed using its index number
// The first character in a string has an index of "0". The next is one and so on.
// console.log(myString[0]);
// console.log(myString[1]);
// console.log(myString[2]);
// console.log(myString[3]);

// This loop prints the individual letters of the myString variable.
for(let x = 0; x < myString.length; x++){
	// The current value of myString is printed out using its index value.
	console.log(myString[x]);
}


/*
	- This loop will print out the letters of the name individually but will print 3 if the ltter is a vowel.
	- This loop starts with the value of zero assigned to i
	- This loop will run as long as the value of i is less than the length of the name.
	- This loop increments
*/
/*let myName = "johndaniel";
for (let i = 0; i< myName.length; i++){
	// If the characters of your name is a vowel letter, instead of displaying the character, it will display the number 3.
	// The "toLowerCase" function
	if (myName[i].toLowerCase() == "a" || myName[i].toLowerCase() == "e" ||
		myName[i].toLowerCase() == "i" ||
		myName[i].toLowerCase() == "o" ||
		myName[i].toLowerCase() == "u"){
		
		console.log(3);
	}
	else{
		console.log(myName[i]);
	}
}*/

for (let count = 0; count <=20; count++){
	
	if (count % 2 === 0){
		continue;
	}

	

	if (count >= 10){
		break;
	}
	console.log("Continue and Break: " + count);
} 

let name = "jakedlexter";

for (let i = 0; i< name.length; i++){
	

	if(name[i].toLowerCase() === "a"){
		console.log("Continue to the next iteration");
		continue;
	}
	console.log(name[i]);
	if (name[i] == "d") {
		break;
	}
}

