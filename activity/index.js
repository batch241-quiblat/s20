let number = Number(prompt("Enter a number"));

console.log("The number you provided is " + number + ".");

for (let i = 0;i <= number; number--){
	if (number <= 50){
		console.log("The current value is at " + number + ". Terminating the loop.");
		break;
	}

	if(number % 10 == 0){
		console.log("The number is divisible by 10. Skipping the number.");
		continue;
	}

	if (number % 5 == 0) {
		console.log(number);
	}
}

let string = "supercalifragilisticexpialidocious";
let consonants="";

for (let i = 0; i < string.length; i++){
	if(string[i] == "a" || string[i] == "e" || string[i] == "i" ||string[i] == "o" || string[i] == "u"){
		continue;
	}else{
		consonants = consonants + string[i];
	}
}

console.log(string);
console.log(consonants);